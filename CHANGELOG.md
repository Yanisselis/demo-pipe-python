# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: Update release process.
- patch: Update release process.

## 0.1.1

- patch: Update release process.

## 0.1.0

- minor: Initial release

